---
layout: handbook-page-toc
title: "Data Engineering Handbook"
description: "GitLab Data Engineering Team Handbook"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

----

# Data Engineering at GitLab

The mission of the Data Engineering team is to build a secure and trusted data platform that makes it possible for **everyone to be an analyst** so that our *only* limitations are the data or analysts themselves. We do this **by means of our** [**GitLab values**](https://about.gitlab.com/handbook/values/) and our [**Data Team Principles**](/handbook/business-ops/data-team/#data-team-principles)

## Data Engineering Responsibilities

Of the [Data Team's Responsibilities](/handbook/business-ops/data-team/#responsibilities) the Data Engineering team is directly responsible for:

- Building and maintaining the company's central Enterprise Data Warehouse to support Reporting, Analysis, Dimensional Modeling, and Data Development for all GitLab teams
- Integrating new data sources to enable analysis of subject areas, activities, and processes
- Building and maintaining an Enterprise Dimensional Model to enable Single Source of Truth results
- Developing Data Management features such as master data, reference data, data quality, data catalog, and data publishing
- Providing Self-Service Data capabilities to help everyone leverage data and analytics
- Help to define and champion Data Quality practices and programs for GitLab data systems
- Providing customizable Data Services, including Data Modeling, Data Quality, and Data Integration

