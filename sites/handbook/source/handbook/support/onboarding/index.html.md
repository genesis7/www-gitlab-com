---
layout: handbook-page-toc
title: Support Onboarding
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# New Hire Onboarding in Support

When you first join the team everything will be new to you. Don't worry! In order to get you started with GitLab quickly, we have an onboarding program that comprises of both company wide and Support specific onboarding.

## Onboarding Timeline & Duration

Typically, for a new Support Engineer, onboarding in GitLab takes **8 weeks**. The following table details the different modules in Support Onboarding Learning Path that will be assigned to you, and how long it typically takes to complete each.

| Issue | Spans | Typically Started In |
| ------ | ------ | ------ |
| [GitLab Onboarding](https://gitlab.com/gitlab-com/people-group/employment-templates/-/blob/master/.gitlab/issue_templates/onboarding.md) | 4 Weeks | Week 1 |
| [Support Engineer Onboarding](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Onboarding%20-%20Support%20Engineer.md) | 4 Weeks | Week 2 |
| [Solutions Focused](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Onboarding%20-%20Solutions%20Focus.md) | 2 Weeks | Week 3 or 4 |
| [Gitlab-dot-com Basics](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Bootcamp%20-%20GitLab.com%20Basics.md) | 2 Weeks | Week 3 or 4 |
| [Customer Emergency Bootcamp](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Bootcamp%20-%20Customer%20Emergencies.md) | 1 Week | Week 6 |
| [CMOC](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Bootcamp%20-%20GitLab.com%20CMOC.md) | 1 Day | Week 6 |

### Note for new hire team member

1. Depending on your focus, you will have to do either Solutions Focused **or** Gitlab-dot-com Basics training.
1. Depending on your focus, you will have to do either Customer Emergency Bootcamp **or** CMOC training.

Your manager will guide you on the above.

## Reference Table for Achievable Progress - First 6 Months

Our onboarding pathway gives new Support Engineers an opportunity to learn at their own pace and explore. We strongly believe in learning by doing ([70/20/10 learning model](https://trainingindustry.com/wiki/content-development/the-702010-model-for-learning-and-development/)), and encourage Support Engineers to start contributing on tickets with public or internal comments as soon as they feel ready. By your 3rd week at GitLab you should have enough to get started!

To help with that, the following reference table can be used as a guideline on what they can aim to achieve in their first few months on tickets. This can also be used by managers as a reference on where they can expect to see their new hire in their first 6 months.

| Month | Monthly Public Comments Reference Range - Self Managed | Monthly Public Comments Reference Range - Dotcom |
| ------ |  ------ | ------ |
| 1 | 0-10 | 0-20 |
| 2 | 10-30 | 20-40 |
| 3 | 30-50 | 40-60 |
| 4 | 50+ | 60+ |
| 5 | 60+ | 80+ |
| 6 | 70+ | 100+ |

## Support Training Project

Whele we are working on building out more learning pathways, you can find a list of all our current bootcamps under the [Support Training project](https://gitlab.com/gitlab-com/support/support-training/-/tree/master/.gitlab/issue_templates).

Please submit an MR if you have suggestions on how this page could be better!