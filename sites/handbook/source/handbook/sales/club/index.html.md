---
layout: handbook-page-toc
title: "GitLab President's Club"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Overview

President’s Club is synonymous with excellence and rewards individuals for achieving specific goals. This event aligns with our values of Collaboration and Results which helps to attract high-performing Sales Reps and other roles that support the sales efforts and allow the company to grow at the expected rate. At GitLab, this reward is in the form of an incentive trip to celebrate the success of our top Field Sales Professionals and others in the organization who helped support the team throughout the year.

![tropical-destination](/handbook/sales/club/tropical2.jpg)

## How to Qualify

For GitLab's FY21 President's Club, 42 GitLab team members (along with 1 guest per team member) will be rewarded with an invitation to join the President's Club incentive trip in May 2021. Team member start dates must be on or before 2020-08-03 to be eligible, and selection criteria varies by role (see below).

## Selection Criteria

| Category | # of winners | Criteria | 
| ------ | ------ | ------ |
| Top Strategic Account Leaders | 8 | based on % of annual full quota (full quota, non-ramped annual quota, prorated based on start date) as shown on the [FY21 President's Club Dashboard](https://app.periscopedata.com/app/gitlab/670929/WIP:-FY21-President's-Club-Dashboard) |
| Top Mid Market Account Executives | 4 | same as above |
| Top SMB Account Executive | 2 | VP/CRO selection (see SMB tab of [this doc](https://docs.google.com/spreadsheets/d/1UMY0rDbSPjw_X9nXKpe7r1SMPuZohgVjVhqEpdGYBJQ/edit?usp=sharing) for details) |
| Top Area Sales Managers | 3 (2 ENT, 1 COM) | based on top quota performance (full quota, non-ramped annual quota, prorated based on start date) as shown on the [FY21 President's Club Dashboard](https://app.periscopedata.com/app/gitlab/670929/WIP:-FY21-President's-Club-Dashboard) |
| Top Enterprise Sales Regional Director | 1 | same as above |
| Top Solution Architects | 8 (includes Channel SAs) | VP/CRO selection (see CS tab of [this doc](https://docs.google.com/spreadsheets/d/1UMY0rDbSPjw_X9nXKpe7r1SMPuZohgVjVhqEpdGYBJQ/edit?usp=sharing) for details) |
| Top Inside Sales Rep | 1 | VP/CRO selection (see ISR tab of [this doc](https://docs.google.com/spreadsheets/d/1UMY0rDbSPjw_X9nXKpe7r1SMPuZohgVjVhqEpdGYBJQ/edit?usp=sharing) for details) |
| Top Channel & Alliances Managers | 3 (2 Channel Sales Managers, 1 Biz Dev/Alliances Manager) | VP/CRO selection (see Channels/Alliances tab of [this doc](https://docs.google.com/spreadsheets/d/1UMY0rDbSPjw_X9nXKpe7r1SMPuZohgVjVhqEpdGYBJQ/edit?usp=sharing) for details) |
| Top Technical Account Managers & Professional Services Engineer | 5 (4 TAMs, 1 PSE) | VP/CRO selection (see CS tab of [this doc](https://docs.google.com/spreadsheets/d/1UMY0rDbSPjw_X9nXKpe7r1SMPuZohgVjVhqEpdGYBJQ/edit?usp=sharing) for details) |
| Top Sales Development Reps | 2 | CMO selection (see SDR tab of [this doc](https://docs.google.com/spreadsheets/d/1UMY0rDbSPjw_X9nXKpe7r1SMPuZohgVjVhqEpdGYBJQ/edit?usp=sharing) for details) |
| Top Sales Development Manager | 1 | CMO selection (see SDR tab of [this doc](https://docs.google.com/spreadsheets/d/1UMY0rDbSPjw_X9nXKpe7r1SMPuZohgVjVhqEpdGYBJQ/edit?usp=sharing) for details) |
| Top CRO Organization Managers | 2 managers in the CRO org that don’t fall under one of the other Club award categories | VP/CRO selection (sales leader nominations to be collected by 1/31/21) |
| Non-Sales MVPs | 2 | VP/CRO selection (sales leader nominations to be collected by 1/31/21) |


Please direct any questions to your manager.
