---
layout: handbook-page-toc
title: "People Experience Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## People Experience Team Availability

Holidays with no availability for onboarding:

| Date                        |
|-----------------------------------|
| 2020-05-01 |
| 2020-12-25 |
| 2021-01-01 |

### OOO Handover Process for People Experience Team

1. The People Experience Associate will create a document the day before their scheduled OOO with the following:
- What urgent tasks are there that need to be reassigned:
* Onboarding
* Offboarding
* Internal Transitions
* Any other tasks
- Is there anything else that we should be made aware of and contribute to whilst you are away?
2. The People Experience Coordinator will then reassign tasks to an alternative People Experience Associate.
3. Get assistance from the People Operation Specialist team if additional hands are needed.

### OOO Process for People Experience Coordinator

1. When the People Experience Coordinator is due to be OOO, the People Experience Associates will allocate the relevant tasks between themselves.
2. In the event that there are items that need urgent attention whilst the People Experience Coordinator is OOO, the Coordinator will hand over the relevant tasks to the People Experience Team Lead or People Experience Associates.

## People Experience Team Processes

### Audits 

- Onboarding
- Offboarding
- Transition
- Probation Period
- Referrals

### HelloSign

As the DRI for HelloSign, when a team member needs to have access to sign documents for the company, an access request needs to be created and assigned to the People Experience Team for provisioning. 

#### Process once Access Request is received:

1. Log into [HelloSign](https://app.hellosign.com/home/) using your personal account information
1. Select `Team` on the left hand-side of the page
1. Insert the team members GitLab email address and click `Invite`
1. Take a screenshot of the confirmed invitation sent and upload to the Access Request as confirmation

#### Monthly Billing

1. When monthly expenses are due, we need to be able to provide Finance with the specific team members name in order to assign to the correct Department. To do this we would need to send a request to HelloSign Support on a monthly basis and often the response is not received in time for the monthly expenses. Once the list is received from HelloSign, simply send the Accounts Payable Specialist an email with the information.  
