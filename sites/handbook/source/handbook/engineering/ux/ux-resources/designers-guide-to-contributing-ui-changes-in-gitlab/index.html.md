---
layout: handbook-page-toc
title: "How to do UI Code Contributions"
---

#### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

Contributing to GitLab's UI requires HTML, CSS, Terminal (CLI), and Git Knowledge. Basic Ruby and JavaScript knowledge is also encouraged.

## Start using Git on the command line

See [Start using Git on the command line](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html) in our GitLab docs for more information.

### Step 1: Install the GDK

The [GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit) (GDK) provides a local GitLab instance that allows you to test changes locally, on your workstation.

1. [Learn how to prepare your workstation to run GDK](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/master/doc/prepare.md)
1. [Learn how to run GDK](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/master/doc/set-up-gdk.md)
1. [GDK Commands cheatsheet](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/master/HELP)

### Step 2: Understand GitLab's codebase structure

Three kinds of files handle UI rendering. At its core, GitLab is a Ruby on Rails application. The Ruby on Rails application renders GitLab's front end with `.haml` files. HAML (HTML Abstraction Markup Language) is a Ruby-based HTML template system. It's easy to learn and it even closes HTML tags for you!

For Stylesheets, GitLab uses a CSS pre-processor called SASS. SASS (Syntactically Awesome Style Sheets) uses `.scss` files that handle all of the usual stuff CSS does, but with a bit more sophistication that helps us keep GitLab's CSS better organized.

Finally, for interactivity and client-side application logic, GitLab uses a framework called Vue.js. It's rare to have to change the `.vue` files unless you're changing a [Pajamas](https://design.gitlab.com/) component, or creating a new one.

### Step 3: Choose a code editor and start making small changes

You can pick any code editor you like, but Visual Studio Code is a safe bet and the industry standard.

Once you have your code editor installed, open the **gitlab-development-kit/gitlab** folder. It contains all of the GitLab files that change when new features and contributions are added.

A good first step is to find some text inside the product to update. Use the **search** feature in your code editor to locate where the text exists in the code base, change the text to something else, and save the file. When you reload your local instance of GitLab, you should see that change reflected in the UI.

### Step 4: Find small issues and open Merge Requests for your changes

Most fixes are a CSS change away. If you can fix it in the browser inspector, you can probably fix it for real in the GitLab codebase. Find small UI issues and submit your changes via [merge requests (MRs)](https://docs.gitlab.com/ee/user/project/merge_requests/). Don't worry, you won't break anything, and a reviewer will always help you check your code before it ships.

## Contributing to GitLab UI

[GitLab UI](https://gitlab.com/gitlab-org/gitlab-ui) is the source for our [Pajamas](https://design.gitlab.com) UI component library. It builds and deploys [Vue.js](https://vuejs.org/) components that are used in some of our GitLab projects, including [gitlab.com](https://gitlab.com/gitlab-org/gitlab). While it's not required of GitLab designers to code or actively contribute to GitLab UI, it is important to have a basic understanding of what it does and how it deploys components that we design.

### GitLab UI basics

* GitLab UI builds and deploys components. We then import those components to use throughout gitlab.com.
* If you've ever created a site or app that utilizes [npm](https://www.npmjs.com/), you [install GitLab UI as a package](https://www.npmjs.com/package/@gitlab/ui) just like you would with other npm packages using `npm install @gitlab/ui`. Since our components are built in Vue, your project also needs to be Vue-based in order to use GitLab UI components.
* We use [Storybook](https://gitlab-org.gitlab.io/gitlab-ui/?path=/story/base-button--default) for development and component documentation, which is then displayed on component pages in [Pajamas](https://design.gitlab.com/).
* We use [visual regression testing](https://gitlab.com/gitlab-org/gitlab-ui#visual-regression-tests) to prevent introducing unexpected regressions with CSS and layout changes on components. After committing changes to a GitLab UI merge request, run the the manual job `update_screenshots` to check for visual regressions and update them automatically:

![Update screenshots manual job](/images/ux/update_screenshots.png){: .small}

### Updating an existing GitLab UI component

If you're updating an existing component, issues may arise if you've changed or removed properties currently in use on gitlab.com. In this case, you'll need to create an integration test merge request on the [gitlab repo](https://gitlab.com/gitlab-org/gitlab) to update existing components that may become broken with your GitLab UI changes. Luckily, this is as simple as running the manual job `create_integration_branch`:

![Create integration branch job](/images/ux/create_integration_branch.png){: .small}

* After running this manual job, click on it and you will see a link to create a merge request from the created integration branch. Open this link to create your merge request:

![Integration branch MR](/images/ux/integration_branch_mr.png)

Once the merge request is open, it will use your GitLab UI branch as the source for GitLab UI, allowing you to update existing components whose properties you may have changed or removed:

![MR gitlab-ui source](/images/ux/mr_gitlabui_source.png)

If you're viewing [Storybook](https://gitlab-org.gitlab.io/gitlab-ui/?path=/story/base-button--default) locally to see your GitLab UI changes, you can select the checkbox `Include GitLab CSS bundle` to pull in the main GitLab CSS file to see if any of your component styles will be overridden. If there are style differences when selecting this checkbox, you may get pipeline failures as a result:

![Include GitLab CSS bundle](/images/ux/include_gitlab_css.png){: .small}

**Note:** Don't be afraid to ask a [GitLab UI maintainer](https://about.gitlab.com/handbook/engineering/projects/#gitlab-ui) for help with setting up your integration test.

### UX showcase video on updating GitLab UI

<iframe width="560" height="315" src="https://www.youtube.com/embed/WWwQ2-UtWRg" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Terminal (CLI) cheatsheet

* [Command Line basic commands](https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html) in our GitLab docs.
* [Basic Git commands](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#basic-git-commands) in our GitLab docs.

## Video walkthrough

Check out this video walkthrough on [Contributing to GitLab (Designer Edition!)](https://www.youtube.com/watch?v=SSo97VwVn4Y&feature=youtu.be) by [Annabel Dunstone Gray](https://gitlab.com/annabeldunstone).

---

This page is adapted from a [beautifully designed PDF](https://gitlab.com/gitlab-org/gitlab-design/-/blob/master/misc/infographics/How_to_Contribute_UI_Code_to_GitLab.pdf) created by [`@jj-ramirez`](https://gitlab.com/jj-ramirez) 😃
