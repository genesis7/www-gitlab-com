---
layout: markdown_page
title: "FY21-Q3 OKRs"
---

This [fiscal quarter](/handbook/finance/#fiscal-year) will run from August 1, 2020 to October 31, 2020.

## On this page
{:.no_toc}

- TOC
{:toc}

## OKR Schedule
The by-the-book schedule for the OKR timeline would be

| OKR schedule | Week of | Task |
| ------ | ------ | ------ |
| -5 | 2020-06-30 | CEO shares top goals with E-group for feedback |
| -4 | 2020-07-06 | CEO pushes top goals to this page |
| -3 | 2020-07-13 | E-group pushes updates to this page |
| -2 | 2020-07-20 | E-group 50 minute draft review meeting |
| -2 | 2020-07-22 | E-group discusses with teams |
| -1 | 2020-07-27 | CEO reports give a How to Achieve presentation |
| 0  | 2020-08-03 | CoS updates OKR page for current quarter to be active |
| +2 | 2020-08-17 | Review previous and next quarter during the next Board meeting |

## OKRs

### 1. CEO: IACV

### 2. CEO: Popular next generation product

### 3. CEO: Great team

## How to Achieve Presentations

* Engineering
* Finance
* Legal
* Marketing
* People
* Product
* Product Strategy
* Sales
