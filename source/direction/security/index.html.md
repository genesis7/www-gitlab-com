---
layout: markdown_page
title: "Section Direction - Security"
---

- TOC
{:toc}

<p align="center">
    <font size="+2">
        <b>Security visibility from development to operations to minimize risk</b>
    </font>
</p>

<!-- GitLab Security intro. -->

![Security Overview](/images/direction/security/security-overview.png)

## Section Overview

GitLab Security Direction is provided by the Sec Product Team.

### Groups

The Security Section is made up of two DevOps stages, Secure and Defend, and seven groups supporting the major categories of DevSecOps including:

* Static Analysis - Assess your applications and services by scanning your source code for vulnerabilities and weaknesses.
* Dynamic Analysis - Assess your applications and services while they are running by leveraging the [Review App](https://docs.gitlab.com/ee/ci/review_apps/) available as part of GitLab’s CI/CD functionality.
* Composition Analysis - Assess your applications and services by analyzing dependencies for vulnerabilities and weaknesses, confirming only approved licenses are in use, and scanning your containers for vulnerabilities and weaknesses.
* Fuzz Testing - Assess your applications and services by inputting unexpected, malformed, and/or random data to measure response or stability by monitoring for unexpected behavior or crashes.
* Container Security - Monitor and protect your cloud-native applications and services by leveraging context-aware knowledge to improve your overall security posture.
* Threat Insights - Holistically view, manage, and reduce potential risks across the entire DevSecOps lifecycle as well as monitor misuse of your GitLab instance.
* Vulnerability Research - Leverage GitLab research to empower your Secure results by connecting security findings to industry references like [CVE IDs](https://cve.mitre.org).

### Resourcing and Investment

## Who are security personas?
TBD

## What's next & why
TBD

### Walkthrough

This is an example of an operations flow we're trying to achieve with incident
management. Note: these are mockups of how we see the future, but the actual
features might look very different or may not ship at all.

![Sec flow](securityflow.gif)

## Target audience

GitLab identifies who our DevSecOps application is built for utilizing the following categorization. We list our view of who we will support when in priority order.
* 🟩- Targeted with strong support
* 🟨- Targeted but incomplete support
* ⬜️- Not targeted but might find value

### Today
To capitalize on the [opportunities](#opportunities) listed above, the Secure Stage has features that make it useful to the following personas today.
1. 🟩 Developers / Development Teams
1. 🟩 Security Teams
1. 🟨 SecOps Teams
1. 🟨 QA engineers / QA Teams
1. ⬜️ Security Consultants

### Medium Term (1-2 years)
As we execute our [3 year strategy](#3-year-strategy), our medium term (1-2 year) goal is to provide a single DevSecOps application that enables collaboration between developers, security teams, SecOps teams, and QA Teams.
1. 🟩 Developers / Development Teams
1. 🟩 Security Teams
1. 🟩 SecOps Teams
1. 🟩 QA engineers / QA Teams
1. 🟨️ Security Consultants

## Key Themes
- [Auto remediation](https://gitlab.com/groups/gitlab-org/-/epics/759)
- [Static application security testing (SAST)](https://gitlab.com/groups/gitlab-org/-/epics/527)
- [Secret detection](https://gitlab.com/groups/gitlab-org/-/epics/675)
- [Bill of materials (dependency list)](https://gitlab.com/groups/gitlab-org/-/epics/858)
- [Runtime application self-protection (RASP)](https://gitlab.com/groups/gitlab-org/-/epics/443)
- [Security dashboard](https://gitlab.com/groups/gitlab-org/-/epics/275)

## Security Section Stage Direction pages

- [Secure](/direction/secure)
- [Defend](/direction/defend)
