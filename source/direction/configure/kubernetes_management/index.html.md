---
layout: markdown_page
title: "Category Direction - Kubernetes Management"
description: "GitLab aim to provide a simple way for users to configure their clusters and manage Kubernetes. Learn more here!"
canonical_path: "/direction/configure/kubernetes_management/"
---

- TOC
{:toc}

## Overview

Kubernetes has become the most widely adopted container orchestration platform to run, deploy, and manage applications.
Yet, to seamlessly connect application code with the clusters in various environments, has remained a complex task that needs various approaches of CI/CD and Infrastructure as Code.

Our mission is to make deployments to every environment frictionless for the developer and reliable for the operator, no matter the experience level. We do this by supporting an integrated experience within GitLab and leverage the existing tools and approaches in the Kubernetes community.

### How you can contribute

Interested in joining the conversation for this category? Please have a look at our
[global epic](https://gitlab.com/groups/gitlab-org/-/epics/115) where
we discuss this topic and can answer any questions you may have. Your contributions are more than welcome.

## Vision

In two years, GitLab is an indispensable addition in the workflow of Kubernetes users by making deployments reliable, frictionless and conventional.

## Target User

We are building GitLab's Kubernetes Management category for the enterprise operators, who are already operating production grade clusters and work hard to remove all the roadblocks that could slow down product development without putting operations at risk.

- [Priyanka](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#priyanka-platform-engineer)

## What's next & why

### GitLab agent

The current version of GitLab Managed Clusters requires `cluster-admin` rights and direct access to the Kubernetes API server to get all the benefits and simplicity GitLab can provide. For serious, enterprise use cases allowing `cluster-admin` rights and K8s API access is not an option. In order to serve our enterprise users we intend to provide an active, cluster-side component to manage deployments and GitLab's cluster integration.

Beside providing more control for the cluster operator, the agent will open up many new possibilities to make GitLab - cluster integrations easier and deeper. We'll try to recognize the applications needed to run various GitLab features and can provide automatic setup of available features.

Please, [join the discussion](https://gitlab.com/gitlab-org/gitlab/-/issues/216569).

### GitLab Managed Applications

GitLab Managed Applications are currently undergoing a large development by moving from the UI-only setup, to a more customizable, CI/CD-based, Infrastructure as Code setup. We want to allow our users to install off the shelf applications with necessary customizations. Moreover, we will continue to offer easy installations of the applications needed to benefit from GitLab's features like the Runner, Auto DevOps, Metrics and log collection, etc. The next step in this journey is to create CI templates for all the applications we support today.

While this approach would allow it, we do not intend to create a marketplace of apps inside of GitLab, instead we would like to integrate with solutions like [OperatorHub](https://operatorhub.io/). As a result, we are looking at providing apps as GitLab Managed Apps if they satisfy the following criteria:

- Each GitLab Managed App should provide sane, production-ready default configurations.
- Each GitLab Managed App should be a requirement for current or planned GitLab functionality or it should integrate deeply with GitLab.
- Each GitLab Managed App should have a clear support path, likely outside of GitLab.

Contribute to these directions in the Epic ["Create CI templates for GitLab-managed-apps"](https://gitlab.com/groups/gitlab-org/-/epics/2103)

## Challenges

We see Kubernetes as a platform for delivering containerised software to users. Currently, every company builds its own workflows on top of it. The biggest challenges as we see them are the following:

- Making Kubernetes friendly to developers is hard. We want to make deployments to every Kubernetes environment frictionless for the developer and reliable for the operator.
- As clusters are complex having an overview of the state and contents of a cluster is hard. We want to help our [monitoring efforts](/direction/monitoring) to provide clean visualisation and a rich understanding of one's cluster.

## Approach

- Kubernetes has a large and engaged community, and we want to build on the knowledge and wisdom of the community, instead of re-inventing existing solutions.
- In Kubernetes there are a plethora of ways for achieving a given goal. We want to provide a default setup and configuration options to integrate with all popular approaches and tools.

## Maturity

Kubernetes Management is currently `Viable`. We want to make it `Complete` by February 2021.

## Competitive landscape

### IBM Cloud Native Toolkit

It provides an Open Source Software Development Life Cycle (SDLC) and complements IBM Cloud Pak solutions. The Cloud Native Toolkit enables application development teams to deliver business value quickly using Red Hat OpenShift and Kubernetes on IBM Cloud.

### Spinnaker

Spinnaker is an open-source, multi-cloud continuous delivery platform that helps you release software changes with high velocity and confidence. It provides two core sets of features 1) Application management, and 2) Application deployment.

## Analyst landscape

This category doesn't quite fit the "configuration management" area as it relates only to Kubernetes. No analyst area currently defined.

## Top Customer Success/Sales issue(s)

[Customize Kubernetes namespace per environment for managed clusters](https://gitlab.com/gitlab-org/gitlab/-/issues/38054)

## Top user issue(s)

[Customize Kubernetes namespace per environment for managed clusters](https://gitlab.com/gitlab-org/gitlab/-/issues/38054)

## Top internal customer issue(s)

[Add more configuration options for launching a new cluster directly from the GitLab application](https://gitlab.com/gitlab-org/gitlab/-/issues/30876)

## Top Vision Item(s)

- [Kubernetes Management - enterprise level K8s integration](https://gitlab.com/gitlab-org/gitlab/-/issues/216569)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/595)
