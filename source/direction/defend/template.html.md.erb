---
layout: markdown_page
title: Product Stage Direction - Defend
description: "GitLab Defend includes all features related to defending your applications and cloud infrastructure, identifying and cataloging threats. Learn more!"
canonical_path: "/direction/defend/"
---

- TOC
{:toc}

<p align="center">
    <font size="+2">
        <b>Protecting cloud-native applications, services, and infrastructure</b>
    </font>
</p>

GitLab Defend enables organizations to proactively protect their cloud-native environments by providing context-aware
technologies to reduce your overall security risk.  Defend is a natural extension of your existing operation&apos;s practices
and provides security visibility across the entire DevSecOps lifecycle.  This empowers your organization
to apply DevSecOps best practices with visibility from the first line of code written all the way through monitoring
and protecting your applications deployed into operations.

![Defend Overview](/images/direction/defend/defend-overview.png)


## Stage Overview

The Defend Stage focuses on providing security visibility across the entire DevSecOps lifecycle as well as providing
monitoring and mitigation of attacks targeting your cloud-native environment.  Defend reduces overall security risk
by enabling you to be ready now for the cloud-native application stack and DevSecOps best practices of the future.
This is accomplished by:

* protecting your applications and services deployed within your cloud-native environment by leveraging context-aware technologies across the entire application stack
* monitoring for misuse within your GitLab deployment&mdash;including its associated runners&mdash;and alerting when anomalies in usage are detected

### Groups

The Defend Stage is made up of two groups supporting the major categories focused on cloud-native security including:

* Container Security - Monitor and protect your cloud-native applications and services by leveraging context-aware knowledge to improve your overall security posture.
* Insider Threat - Monitor and protect against misuse of your GitLab deployment at the project, group, and instance level including SCM actions and CI/CD pipeline runners.

### Resourcing and Investment

The existing team members for the Defend Stage can be found in the links below:

* [Development](https://about.gitlab.com/company/team/?department=defend-section)
* [User Experience](https://about.gitlab.com/company/team/?department=defend-ux-team)
* [Product Management](https://about.gitlab.com/company/team/?department=defend-pm-team)
* [Quality Engineering](https://about.gitlab.com/company/team/?department=secure-enablement-qe-team)

## 3 Year Stage Themes

### Cloud native first

Defend will focus first on providing reactive security solutions that are cloud native.  Whenever possible, Defend capabilities will be cloud platform agnostic, including support for self-hosted cloud native environments.

### Visibility first, protection second

Defend will focus on detecting and alerting on malicious traffic and activity as a first step followed
by introducing prevention as a second step. This is valuable to you because it means that you can
introduce Defend within your cloud native applications, services, and infrastructure gradually over
time without disrupting your end users. This allows you to examine the alerts generated by Defend
using detection policies to ensure all security settings are appropriate and accurate for your cloud
native deployment prior to moving to protection policies. This allows you to eliminate false
positives and prevents your users from having a poor user experience with your applications and
services. Furthermore, Defend will provide you with evidence of malicious traffic and activity as
well as any actions taken, allowing you to meet and exceed your compliance goals and requirements.

As examples, GitLab will provide:
* Allow mode first, allowing your organization to have security visibility within your cloud native environment.
* Block mode second, allowing your organization to take action on malicious traffic and activities.
* Logging of all events, both internally to your cluster as well as to an external SIEM
(like [GitLab Monitor](https://about.gitlab.com/stages-devops-lifecycle/monitor/)).
* Alerts, notifications, and workflows for responding to those Alerts including the ability to, as an example, create GitLab issues or
send ChatOps notifications automatically.


### Blur the line between development and security operations

One of the key advantages to GitLab being a single application for the entire DevOps lifecycle is that
all of our stages are tightly integrated. This empowers Defend to both provide information into
other stages, such as Plan, as well as to receive information from other stages, such as Secure.

Defend will leverage knowledge from the Secure Stage and provide virtual patching of security policies within Defend Categories.
Additionally, Defend will eventually leverage Secure scanning capabilities to provide on-going scanning of applications after
they have been deployed to production.  This allows for detection of any new threat vectors or vulnerabilities that were published
after the original push to production.

Defend will identify and protect against threats as they happen, and will also strive to provide actionable next steps
to close a vulnerability or point of exploit, not just defend it.

Not only does shifting left and acting on results earlier give your apps better security, it helps
[enable collaboration](/handbook/product/product-principles/#enabling-collaboration) with everyone at your company.
We believe that security is everyone&apos;s responsibility and that [everyone can contribute](/company/strategy/#mission).
Informing other stages is a powerful way to do this.

As examples, GitLab will provide:
* Suggested code corrections or package updates to fix vulnerabilities at the source
* Suggested changes to access controls or permissions to better follow the [Principle of Least Privilege](https://en.wikipedia.org/wiki/Principle_of_least_privilege)

### Improve OSS security standards

Defend will leverage OSS security projects to build our solutions.  Defend will contribute improvements to these projects upstream helping everyone be more secure.

For example, GitLab recently added a [Policy Audit Mode](https://cilium.io/blog/2020/06/22/cilium-18/#policy-audit-mode) to the upstream open source [Cilium](https://cilium.io/) project to allow users to test their policies before enforcing them.

### Emphasize usability and convention over configuration
Defend capabilities will be pre-configured to provide value to protecting your applications.
Rather than require you to read documentation manuals and provide complex configuration files,
[GitLab will always provide reasonable defaults](/handbook/product/product-principles/#convention-over-configuration)
out of the box.

We will provide the ability for advanced and customized configurations, but these will
only be needed based on your specific use case and when you feel comfortable doing so.

## 3 Year Strategy
Effectively protecting applications running in production requires the following key software capabilities that will be developed over the next 3 years:
1. A streamlined workflow for triaging, investigating, and mitigating application attacks.  This workflow will be tightly integrated into the rest of the GitLab product.
1. Seamless two-way integrations with other security products to both allow GitLab to share alerts with other tools as well as to allow other tools to share information about attacks with GitLab.
1. A best-in-class policy editor that is both powerful and easy to use.  By providing clear information about what policies are deployed, misconfigurations and accidental security holes can be avoided.
1. Analytics that save time and reduce errors by leveraging GitLab&apos;s knowledge of the application code to reduce false positives by tuning policies, effectively prioritizing alerts, and auto-suggesting security best practices.
1. In-depth introspection into the overhead, uptime, and performance of Defend technologies to ensure that security protections are always up and running without interfering with application performance.
1. Enterprise-grade user permissions, audit logging, change control, and reporting to ensure that the GitLab application itself is safe from threats.
1. Managed Security Service Provider (MSSP) support to allow partners to effectively and efficiently monitor the security posture of their clients&apos; applications.


## Pricing
Defend is focused on providing enterprise-grade security capabilities to protect production environments.  The intent is to provide enough value in paid features so as to allow Defend to contribute in a significant way over the next 3 years toward GitLab&apos;s [company-level financial goals](https://about.gitlab.com/company/okrs/).

Although paid features are the primary focus, there are several reasons why features for unpaid tiers might be prioritized above paid features:
1. The Defend stage has a powerful potential to help improve the security of projects everywhere.  Features with the potential to significantly improve the secuity posture of all projects will be highly prioritized and made available in a unpaid tier.
1. To continue to be [good stewards in the open source community](https://about.gitlab.com/company/stewardship/), any basic integration with open source projects will be available in an unpaid tier, along with the "table stakes" set of functionality required to allow that feature to be usable with GitLab.

### Core/Free
This tier is the primary way to increase broad adoption of the Defend stage, as well as encouraging community contributions and improving security across the entire GitLab user base.

As a general rule of thumb, features will fall in the Core/Free tier when they meet one or more of the following criteria:
* The feature is highly useful for an individual with a few small projects rather than meeting the needs of an organization or enterprise that is operating at scale
* The feature is provided by an integration with an open source project rather than being natively developed by GitLab
* The feature has the potential to increase the security posture of all GitLab projects in a significant way, or there is a reasonable ethical/moral obligation to make the feature available to all users

Some examples include:
* Basic installation and use of the security capabilities, especially when the underlying technology leverages an open source project
* The ability to collect security logs
* Documentation and directions on how to configure policies for those security tools (this may require use of the command line)

### Starter/Bronze
This tier is not a significant part of Defend&apos;s pricing strategy.

### Premium/Silver
This tier is not a significant part of Defend&apos;s pricing strategy.

### Ultimate/Gold
This tier is the primary focus for the Defend stage as the security posture of an organization is typically a primary concern of the executive team and even the board of directors.  Just as a major security incident has far reaching consequences that impact the entirety of an organization, the features and capabilities that successfully protect against those types of attacks tend to be valued with equal weight.  The types of capabilities that fall in the Ultimate/Gold tier vs an unpaid tier should be those that are necessary for an organization, rather than an individual, to provide for adequate security of their production environment.

As a general rule of thumb, features will fall in the Ultimate/Gold tier when they meet one or more of the following criteria:
* The feature is focused on enabling an organization or enterprise to operate at scale rather than an individual with a few small projects
* The feature is natively developed by GitLab rather than being provided by an open source project

Some examples include:
* A dashboard to aggregate alerts, along with analytics to prioritize those alerts and a workflow for responding to those alerts
* A UI-driven policy management editor, along with any analytics that are built to auto-suggest or auto-tune policies
* Introspection into the up-time and performance of the security tools, along with any auto-generated recommendations for tuning the configuration of those tools
* Enterprise-grade permissions and audit logging to monitor and limit any single individual&apos;s ability to make changes


<%= partial("direction/categories", :locals => { :stageKey => "defend" }) %>

## Upcoming Releases

<%= direction["all"]["all"] %>

<%= partial("direction/other", :locals => { :stage => "defend" }) %>
