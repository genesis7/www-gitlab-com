---
layout: markdown_page
title: "Category Direction - Jira Importer"
description: This is the category direction for Jira Importer in GitLab; which is part of the Plan stage's Project Management group.
canonical_path: "/direction/plan/jira_importer/"
---

Last reviewed: 2020-04-13

- TOC
{:toc}

## 🚥 Jira Importer

|          |                                |
| -------- | ------------------------------ |
| Stage    | [Plan](/direction/plan/)       |
| Maturity | [Planned](/direction/maturity/) |

### Introduction and how you can help

<!-- Introduce yourself and the category. Use this as an opportunity to point users to the right places for contributing and collaborating with you as the PM -->

<!--
<EXAMPLE>
Thanks for visiting this category strategy page on Snippets in GitLab. This page belongs to the [Editor](/handbook/product/product-categories/#editor-group) group of the Create stage and is maintained by <PM NAME>([E-Mail](mailto:<EMAIL@gitlab.com>) [Twitter](https://twitter.com/<TWITTER>)).

This strategy is a work in progress, and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=snippets) and [epics]((https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=snippets) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your need for snippets, we'd especially love to hear from you.
</EXAMPLE>
-->

👋 This is the category direction for Jira Importer in GitLab; which is part of the Plan stage's [Project Management](/handbook/categories/#project-management-group) group. Please reach out to the group's Product Manager, Gabe Weaver ([E-mail](mailto:gweaver@gitlab.com)), if you'd like to provide feedback or ask any questions related to this product category.

This category direction is a work in progress and everyone can contribute:

- Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aproject%20management&label_name[]=Category%3AJira%20Importer) and [epic](https://gitlab.com/groups/gitlab-org/-/epics/2738) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
- Please share feedback directly via email, Twitter, or on a video call.

### Overview

Jira is one of the most common project and portfolio managmenet tools with roughly 34% market share. Many GitLab customers that purchase GitLab for source code management
or CI/CD functionality continue to use Jira due to:

- The lack of an easy migration path from Jira to GitLab.
- [Missing capabilities](https://gitlab.com/groups/gitlab-org/-/epics/2586) within GitLab that are necessary for teams to plan and manage their work effectively.

We are focusing on providing a great Jira importing experience in lock step with improving GitLab's planning capabilities. It is our goal to make it effortless for our customers to start realizing the increased value and efficiencies that are unlocked when project and product management is colocated within the same application as the rest of the DevOps lifecycle.

### Where we are Headed

#### What's Next & Why

<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/product-management/process/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.-->

These are the epics and issues that we will be working on over the next few releases:

- **Shipped in 12.10** - Leverage our existing Jira Service Integration to deliver an [MVC](https://gitlab.com/groups/gitlab-org/-/epics/2766) Jira Project > GitLab Project importing experience.
  - **Outcome:** Project Admins/Owners can import basic issue data (Title, Description) from a Jira to Gitlab
- Extend the importer to support additional data mapping as outlined in the [follow-up iterations](https://gitlab.com/groups/gitlab-org/-/epics/2738#follow-up-iterations-aka-what-the-mvc-doesnt-do). We are currently working towards supporting [mapping users from Jira to GitLab](https://gitlab.com/gitlab-org/gitlab/-/issues/210580) so imported comments and issue activity are correctly attributed to the author. We are also focusing on [building a better parser](https://gitlab.com/gitlab-org/gitlab/-/issues/212295) to handle converting Jira issue descriptions to GitLab Flavored Markdown.
  - **Outcome:** Jira issues can be imported to GitLab without losing contextual information such as comments and their original authors, issue dependencies and relationships, and epics. We provide verification that 100% of the targeted issues were successfully imported into GitLab.

#### What is Not Planned Right Now

- Importing complex workflows, triggers, and automations from Jira to GitLab. As GitLab's capabilities mature, we will revisit whether or not this would provide additional value for teams looking to move from Jira to GitLab.
- Importing Confluence wikis to GitLab wikis. While we intend on supporting this at some point in the future, we are currently focused on creating a Jira Software importer that helps teams transition their issue management to GitLab.

#### Maturity Plan

<!-- It's important your users know where you're headed next. The maturity plan
section captures this by showing what's required to achieve the next level. The
section should follow this format:

This category is currently at the XXXX maturity level, and our next maturity target is YYYY (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/product-categories/maturity/#legend)).

- Link to maturity epic if you are using one, otherwise list issues with maturity::YYYY labels) -->

This category is currently at the 😊**Minimal** maturity level. We are tracking our progress against becoming 😁**Viable** via [this epic](https://gitlab.com/groups/gitlab-org/-/epics/2738).

### User success metrics

- 100% of targeted issues are imported successfully from Jira into GitLab.
- Count of total issues imported.
- Count of projects that imported issues from Jira to GitLab.
- Count of instances using the Jira issue importer.

### Top Customer Success/Sales issue(s)

- None at this time.

### Top user issue(s)

- [Jira Importer](https://gitlab.com/groups/gitlab-org/-/epics/2738) (👍 44, +0)

### Top internal customer issue(s)

- Not applicable as we use GitLab for issue management.

### Top Strategy Item(s)

This is a non-marketing category which was created as a specific strategy to help drive our 1 year plan of [importing from Jira without losting required data](https://about.gitlab.com/direction/dev/#plan). While the importer itself is an important component of this, we also need to maintain [focus on adding capabilities](https://gitlab.com/groups/gitlab-org/-/epics/2586) to GitLab that enable us to win successfully against competitors like Jira.
