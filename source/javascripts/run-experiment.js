// Define the namespace
var runExperiment = function(issueId, featureFlag, experimentControlExtraFunctions, experimentTestExtraFunctions, featureFlagEnvironment, experimentCookieDuration = 1209600)
{
  // The above function parameter "experimentCookieDuration" is defined in seconds, 1 day = 86400, 1 week = 604800, 2 weeks = 1209600, 28 days = 2419200. The default above is 2 weeks.

  // Developer mode settings
  var consoleLogsEnable = new Boolean(false);
  var globalEnvironmentOverride = '';

  // Define variables & configurations
  var experimentWrapperId = '#experiment' + issueId;
  var experimentTargetControl = experimentWrapperId + ' .experiment-control';
  var experimentTargetTest = experimentWrapperId + ' .experiment-test';
  var experimentLoading = experimentWrapperId + ' .loading-experiment';
  var controlExtras = experimentControlExtraFunctions;
  var testExtras = experimentTestExtraFunctions;
  var environmentValue = featureFlagEnvironment;
  var production = '5e4333e30437cc080f58513a';
  var test = '5e4333e30437cc080f585139';
  var dev = '5e4f2535a16fb207f4e766fd';
  var environmentKey;

  // Process the environment configuration
  if(globalEnvironmentOverride === 'production'){environmentKey = production;}
  else if(globalEnvironmentOverride === 'test'){environmentKey = test;}
  else if(globalEnvironmentOverride === 'dev'){environmentKey = dev;}
  else if(environmentValue === 'production'){environmentKey = production;}
  else if(environmentValue === 'test'){environmentKey = test;}
  else {environmentKey = dev;}
  if(consoleLogsEnable == true)
  {
    console.log('feature flag environment: ' + environmentValue);
    console.log('override environment: ' + globalEnvironmentOverride);
    console.log('actual environment id: ' + environmentKey);
  };

  // Function for assigning a UUID per https://stackoverflow.com/q/105034
  function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  };

  // Function for retrieving the cookie value
  function getCookie(name) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop().split(';').shift();
  }

  // IMPORTANT NOTES
  // 1. Before checking if the flag is enabled, we have to assign a UUID.
  // 2. Before assigning a UUID (1), we have to check if the user already has an ID so we don't break any preexisting assignments.
  // 3. To determine if a user has a UUID (2), we have to look for or set a cookie.

  function getExperimentUuid() {
    // Check for preexisting UUID (cookie)
    var launchdarklyUuid = getCookie('experimentUuid');
    if(consoleLogsEnable == true)
    {
      console.log('preexisting experimentUuid: ' + launchdarklyUuid);
    };
    if (launchdarklyUuid !== undefined)
    {
      return launchdarklyUuid;
    } else {
      // If no preexisting UUID, then assign one and create a cookie
      launchdarklyUuid = uuidv4();
      if(consoleLogsEnable == true)
      {
        console.log('assigned experimentUuid: ' + launchdarklyUuid);
        console.log('experimentUuid duration: ' + experimentCookieDuration);
      };
    };
    var assembleCookie = 'experimentUuid=' + launchdarklyUuid + '; max-age=' + experimentCookieDuration;
    document.cookie = assembleCookie;
    return launchdarklyUuid;
  };
  var experimentUuid = getExperimentUuid();

  // Initialize LaunchDarkly
  var user = {'key': experimentUuid};
  var ldclient = LDClient.initialize(environmentKey, user);

  // Define what should happen
  function render()
  {

    // Init the feature flag object
    var isThisTheTestVariant = ldclient.variation(featureFlag, false);
    if(consoleLogsEnable == true)
    {
      console.log('issue id: ' + issueId + ', flag name: ' + featureFlag + ', isThisTheTestVariant: ' + isThisTheTestVariant);
    };

    // Show, hide, and run extra functions.
    if(isThisTheTestVariant)
    {
      // THIS IS THE TEST VARIANT

      // TIMEOUTS: wait 150 ms to poll isThisTheTestVariant
      setTimeout(function()
      {
        document.querySelector(experimentLoading).classList.add('loaded-experiment');
        document.querySelector(experimentTargetControl).classList.remove('experiment-visible');
        document.querySelector(experimentTargetTest).classList.add('experiment-visible');
      }, 150);

      if(experimentTestExtraFunctions !== null)
      {
        // TIMEOUTS: wait another 200 ms for the fade animation to finish (total 350ms)
        setTimeout(function()
        {
          testExtras();
          if(consoleLogsEnable == true)
          {
            console.log('test extras: ' + testExtras);
          };
        }, 350);
      };

    } else {
      // THIS IS THE CONTROL VARIANT

      // TIMEOUTS: wait 100 ms to poll isThisTheTestVariant
      setTimeout(function()
      {
        document.querySelector(experimentLoading).classList.add('loaded-experiment');
        document.querySelector(experimentTargetTest).classList.remove('experiment-visible');
        document.querySelector(experimentTargetControl).classList.add('experiment-visible');
      }, 150);

      if(experimentControlExtraFunctions !== null)
      {
        // TIMEOUTS: wait another 200 ms for the fade animation to finish (total 350ms)
        setTimeout(function()
        {
          controlExtras();
          if(consoleLogsEnable == true)
          {
            console.log('control extras: ' + controlExtras);
          };
        }, 350);
      };
    };
  };
  // Subscribe to the feature flag and then run the functions based on it's state
  ldclient.on('ready', function()
  {
    if(consoleLogsEnable == true)
    {
      console.log('feature flag system is ready');
    };
    render();
  });
  /*
  // commented out for now. typically we don't need this to operate on change.
  ldclient.on('change', function()
  {
    if(consoleLogsEnable == true)
    {
      console.log('feature flag "' + featureFlag + '" status has changed');
    };
    render();
  });
  */
};
